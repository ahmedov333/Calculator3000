package net.calculator;

import static jdk.nashorn.internal.objects.Global.Infinity;

/**
 * Created by slebedev on 16.05.2017.
 */
public class ExpressionParser {
    private String str;

    int pos = -1, ch;

    private void nextChar() {
        ch = (++pos < str.length()) ? str.charAt(pos) : -1;
    }

    private boolean eat(int charToEat) {
        while (ch == ' ') nextChar();
        if (ch == charToEat) {
            nextChar();
            return true;
        }
        return false;
    }

    public Double parse(String str) {
        this.str = str;
        nextChar();
        double x = parseExpression();
        if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
        if(x == Infinity) throw new ArithmeticException("Division by zero!");
        return x;
    }


    // Grammar:
    // expression = term | expression `+` term | expression `-` term
    // term = factor | term `*` factor | term `/` factor
    // factor = `+` factor | `-` factor | `(` expression `)`
    //        | number

    double parseExpression() {
        double x = parseTerm();
        for (; ; ) {
            if (eat('+')) x += parseTerm(); // addition
            else if (eat('-')) x -= parseTerm(); // subtraction
            else return x;
        }
    }

    double parseTerm() {
        Double x = parseFactor();
        for (; ; ) {
            if (eat('*')) x *= parseFactor(); // multiplication
//                    else if (eat('/')&& eat('0')) System.out.println("error");
            else if (eat('/')) x /= parseFactor(); // division
            else if (eat('!')) {
                if(!isFractionalNull(x)) throw new IllegalArgumentException("Number contains . !");
                x = new Double(factorial(x.intValue()));
            }// factorial
            else return x;
        }
    }

    double parseFactor() {
        if (eat('+')) return parseFactor(); // unary plus
        if (eat('-')) return -parseFactor(); // unary minus

        Double x;
        int startPos = this.pos;
        if (eat('(')) { // parentheses
            x = parseExpression();
            eat(')');
        } else if ((ch >= '0' && ch <= '9') || ch == '.' ) { // numbers
            while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
            x = Double.parseDouble(str.substring(startPos, this.pos));
        }
        else {
            throw new RuntimeException("Unexpected: " + (char) ch);
        }
        return x;
    }

    public int factorial(int n)
    {
        if (n == 0) return 1;
        return n * factorial(n-1);
    }

    private boolean isFractionalNull(double number){
        int decimal = (int) number;
        return (number - decimal) == 0;
    }
}
