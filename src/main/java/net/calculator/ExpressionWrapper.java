package net.calculator;

/**
 * Created by Sergei on 23.05.2017.
 */
public class ExpressionWrapper {
    public static String eval(final String expression){
        String result = null;
        try{
            result = new ExpressionParser().parse(expression).toString();
        }catch (Exception e){
            result = e.getMessage();
        }
        return result;
    }
}
