package net.calculator;

/**
 * Created by Sergei on 22.05.2017.
 */
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/main")
public class MainController {
    @RequestMapping(value = "/expression", method = RequestMethod.GET, produces = "application/json")
    public String greeting(@RequestParam(value="expression") String expression) {
        return JSONObject.quote(ExpressionWrapper.eval(expression));

    }
}
