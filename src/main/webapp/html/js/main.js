var Calculator = {

    results_id:    'calculator-result',
    results_value: '0',
    memory_id:     'calculator-screen',
    memory_value:  '',

    SUM:  ' + ',
    MIN:  ' - ',
    DIV:  ' / ',
    MULT: ' * ',
    PROC: '%',
    SIN:  'sin(',
    COS:  'cos(',
    MOD:  ' mod ',
    BRO:  '(',
    BRC:  ')',

    sendExpression:function(value){
        var result;
        $.ajax({
            async:false,
            type: "get",
            url: "main/expression",
            dataType:"json",
            data: {expression: value},
            success: function (response) {
               result =  response;
            },
            error: function (response) {
                result =  response;
            }
        });
        return result;
    },

    calculate: function() {
        this.results_value = this.sendExpression(this.memory_value);
        this.refresh();
    },

    put: function(value) {
        this.memory_value += value;
        this.update_memory();
    },

    reset: function() {
        this.memory_value = '';
        this.results_value = '0';
        this.refresh();
    },

    refresh: function() {
        this.update_result();
        this.update_memory();
    },

    update_result: function() {
        document.getElementById(this.results_id).innerHTML = this.results_value;
    },

    update_memory: function() {
        document.getElementById(this.memory_id).innerHTML = this.memory_value;
    },

    engine: {
        exec: function(value) {
            try {return eval(this.parse(value))}
            catch (e) {return "Syntax error"}
        },

        parse: function(value) {
            if (value != null && value != '') {
                value = this.replaceFun(value, Calculator.PROC, '/100');
                value = this.replaceFun(value, Calculator.MOD, '%');
                value = this.addSequence(value, Calculator.PROC);

                value = this.replaceFun(value, 'sin', 'Math.sin');
                value = this.replaceFun(value, 'cos', 'Math.cos');
                return value;
            }
            else return '0';
        },

        replaceFun: function(txt, reg, fun) {
            return txt.replace(new RegExp(reg, 'g'), fun);
        },

        addSequence: function(txt, fun) {
            var list = txt.split(fun);
            var line = '';

            for(var nr in list) {
                if (line != '') {
                    line = '(' + line + ')' + fun + '(' + list[nr] + ')';
                } else {
                    line = list[nr];
                }
            }
            return line;
        }
    }
}

$(document).keypress(function(e) {
    var element = $('*[data-key="'+e.which+'"]');

    var fun = function(element){
        // skip if this is no a functional button
        if (element.length == 0){ return true }

        if (element.data('constant') != undefined){
            return Calculator.put(Calculator[element.data('constant')]);
        }

        if (element.data('method') != undefined){
            return Calculator[element.data('method')]();
        }

        return Calculator.put(element.html());
    }

    if (fun(element) != false){
        return false
    } else {
        return true
    }
});

$(document).ready(function() {

    $(".btn").click(function(e) {
        e.preventDefault();

        if ($(this).data('constant') != undefined){
            return Calculator.put(Calculator[$(this).data('constant')]);
        }

        if ($(this).data('method') != undefined){
            return Calculator[$(this).data('method')]();
        }

        return Calculator.put($(this).html());
    });
});
