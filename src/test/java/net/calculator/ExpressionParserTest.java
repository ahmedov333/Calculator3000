package net.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by slebedev on 16.05.2017.
 */
@RunWith(JUnit4.class)
public class ExpressionParserTest {
    @Test
    public void evalTest() throws Exception {
        Assert.assertEquals((Double)2.0, new ExpressionParser().parse("4/2"));
    }

    @Test(expected = RuntimeException.class)
    public void evalTestWrongExpression() throws Exception {
        new ExpressionParser().parse("4//2");
    }

    @Test(expected = ArithmeticException.class)
    public void evalTestNullDivision() throws Exception {
        new ExpressionParser().parse("4/0");
    }

    @Test
    public void evalTestFactorial() throws Exception {
        Assert.assertEquals((Double)6.0, new ExpressionParser().parse("3!"));
        Assert.assertEquals((Double)9.0, new ExpressionParser().parse("3!+3"));
        Assert.assertEquals((Double)3.0, new ExpressionParser().parse("3!/2"));
        Assert.assertEquals((Double)12.0, new ExpressionParser().parse("3!*2"));
        Assert.assertEquals((Double)1.0, new ExpressionParser().parse("0!"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void evalTestFactorialInvalidNumber() throws Exception {
        new ExpressionParser().parse("3.2!");
    }
}